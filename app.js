var express = require("express");
const cors = require('cors');

let app = express();
const port = process.env.PORT || 8887;

app.use(express.urlencoded({ extended: true })); //middleware portion for adding data
app.use(cors());
app.use(express.json());



const path = require('path'); //* The path module provides utilities for working
                              //* with file and directory paths.

app.use(express.static('./dist/FrontEnd')); //*To serve static files such as images, CSS files,
                                            //* and JavaScript files, use the express.static built-in 
                                            //*middleware function in Express.



//! < All your  routes should be included here>
app.get("/api/test", function (req, res) {
    console.log("entered inside api/test")
    res.send({ status: true, message: "Hello" })
});



app.get('/*', function (req, res) {  //* Primary

    res.sendFile(path.join(__dirname + '/dist/FrontEnd/index.html')); //*using path module here.
    //*Wait for a request to any path and redirect all of the requests to index.html
   //! This is last/end route

});








app.listen(port, () => {
    console.log("Server ready at" + port)
});
